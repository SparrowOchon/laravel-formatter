# Laravel Formatter

Installation code to setup formatter for a laravel project.<br>
This will automatically format using:

Prettier:

- Js
- Vue
- Html
- Css
- Blade

Php-cs-fixer

- php

**Blade files** are a template file for Laravel as such prettier will format it as html
formatting all the html code. Php-cs-fixer will then fromat it as a php file
leaving the html code untouched.

## Usage

2 Methods of usage are provided:

1. Composer.json file to allow for formatting using `composer format` this requires the composer.json file to be placed at the root of the project.

2. Package.json file this allows for the Laravel package formatting using npm. By adding the package.json file to the root of the laravel project. `DIR=path/to/format npm run format`.

## Installation

1. Run the `install <composer | npm > <PATH/TO/PROJECT>` script.
   The script will automatically add the composer.json or package.json changes needed based on the path to the project

#!/bin/bash
#================================================================================================
#
# FILENAME :        install.sh
#
# DESCRIPTION : Install the formatters used as standard enforcement for laravel
#
# USAGE:
#				./install.sh <composer|npm> <Path/to/Codebase>
#
#
# AUTHOR :   Network Silence        START DATE :    28 July 2019
#
#
# LICENSE :
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
#	Copyright (c) 2019, Network Silence
#		All rights reserved.
#
#
#
#
#===============================================================================================
PASS=""
composer_config="composer.json"
npm_config="package.json"

function main(){
	local project_path=$2
	local formatter_type=$1
	is_user_sudo "$@"
	get_sudoer_pass
	echo $PASS | sudo -S apt-get install composer nodejs -y
	echo $PASS | sudo -S npm install --global prettier
	working_dir=$(validate_dir $project_path)
	cd $working_dir
	composer require friendsofphp/php-cs-fixer
	export PATH="$PATH:$HOME/.composer/vendor/bin"
	setup_formatters $formatter_type $project_path
}
function is_user_sudo(){
	if [[ $(id -u) = 0 ]]; then
		print_error "Do not run this as sudo."
	elif [[ $# -ne 2 ]]; then
		print_error "Parameters missing."
	fi
}
function get_sudoer_pass(){
	while [[ $PASS == "" ]]; do
		sudo -k
		echo -n "Enter the sudo password:"
		read temppass
		echo $temppass | sudo -S echo testpass &> /dev/null
		if ! [ "$(sudo -n echo testpass 2>&1)" == "testpass" ]; then
			echo "Incorrect password was entered"
		else
			PASS=$temppass
		fi
	done
}
function print_error(){
	echo "USAGE: $0 <composer|npm> <DIR OF PROJECT>"
	echo $1
	exit 1
}
function validate_dir(){
	local dir_to_test=$1
	if [[ -d $dir_to_test ]]; then
		echo $dir_to_test
	else
		print_error "Invalid directory $dir_to_test"
	fi
}
function setup_formatters(){
	local project_path=$2
	local setup_type=$1
	local formatter_string=""

	if [[ $setup_type == "composer" && -f $project_path/$composer_config ]]; then
		formatter_string=$(awk '/format/,/]/' $composer_config)
		$(append_to_config $project_path/$composer_config $formatter_string)

	elif [[ $setup_type == "npm" && -f $project_path/$npm_config ]]; then
		formatter_string=$(awk '/format/,/"/' $npm_config)
		$(append_to_config $project_path/$npm_config $formatter_string)
	else
		print_error "Unable to access $setup_type config in $project_path"
	fi
}
function append_to_config(){
	local destination_config=$1
	local formatter_string=$2

	if [[ $(grep "scripts: {" $destination_config) ]]; then
		# Append to existing Scripts
		sed -i "s/\"scripts\": {/\"scripts\": {\n 	$formatter_string,/g" $destination_config
	else
		# Create new Scripts
		sed -i "s/},/},\n \"scripts\": {\n $formatter_string },/g" $destination_config
	fi
}
main "$@"
